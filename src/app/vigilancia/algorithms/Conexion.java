
package app.vigilancia.algorithms;

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Alexis
 */
public class Conexion {
    
    Connection conexion;
    String servidor="jdbc:mysql://localhost/";
    String DB="sistemaseguridad";
    String userDB="root";
    String passDB="xperiax8";

    
    public Connection getConexion(){        
        try {
            conexion=(Connection) DriverManager.getConnection(servidor,userDB,passDB);
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return conexion;
    }
}


