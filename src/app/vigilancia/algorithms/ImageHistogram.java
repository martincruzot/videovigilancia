
package app.vigilancia.algorithms;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

public class ImageHistogram {
    
    private Image image;
    private BufferedImage bufferedImage;
    private int[] redChannel    = new int[256];;
    private int[] greenChannel  = new int[256];;
    private int[] blueChannel   = new int[256];;
    private int[] alphaChannel  = new int[256];;
    private int[] grayScale     = new int[256];;
    private int[] patterns      = new int[768];
    
    public ImageHistogram(Image image) {
        this.image = image;
        bufferedImage = toBufferedImage(image);
        this.generateHistogram(bufferedImage);
    }
    
    public static BufferedImage toBufferedImage(Image img){
        if (img instanceof BufferedImage) {
            return (BufferedImage) img;
        }
        // Create a buffered image with transparency
        BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);
        // Draw the image on to the buffered image
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();
        // Return the buffered image
        return bimage;
    }
    
    private int RGBToGray(Color c) {
        return (c.getRed() + c.getBlue() + c.getGreen()) / 3;
    }

    private void generateHistogram(BufferedImage image) {

        this.image = image;
        Color c;

        for (int i = 0; i < image.getWidth(); i++) {
            
            for (int j = 0; j < image.getHeight(); j++) {

                c = new Color(image.getRGB(i, j));

                redChannel[c.getRed()] ++;
                greenChannel[c.getGreen()] ++;
                blueChannel[c.getBlue()] ++;
                alphaChannel[c.getAlpha()] ++;
                grayScale[this.RGBToGray(c)] ++;
            }
        }
        
        // El patron es la union de los 3 vectores (histogramas)
        System.arraycopy(redChannel, 0, patterns, 0, redChannel.length);
        System.arraycopy(greenChannel, 0, patterns, redChannel.length, greenChannel.length);
        System.arraycopy(blueChannel,  0, patterns, redChannel.length + greenChannel.length, blueChannel.length);

    }    
}
