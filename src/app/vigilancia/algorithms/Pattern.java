package app.vigilancia.algorithms;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.math.BigDecimal;

public class Pattern {

    private Image image;
    private BufferedImage bufferedImage;
    private double patrones[] = new double[9];

    private double[][] matrizImg;	    //matrizImg de la imagen en escala de grises
    private double[][] matrizImg_R;         //matrizImg de la imagen en R
    private double[][] matrizImg_G;         //matriz de la imagen en G
    private double[][] matrizImg_B;         //matriz de la imagen en B
    private int filas;
    private int columnas;

    public Pattern(Image image) {
        this.image = image;
        bufferedImage = (BufferedImage) image;
        convertirImagenAMatriz();
        this.generateSegmets(bufferedImage);
    }

    private static BufferedImage toBufferedImage(Image img) {
        if (img instanceof BufferedImage) {
            return (BufferedImage) img;
        }
        // Create a buffered image with transparency
        BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);
        // Draw the image on to the buffered image
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();
        // Return the buffered image
        return bimage;
    }

    private void generateSegmets(BufferedImage bufferedImage) {
     
        double TF = filas / 3;  // tercia filas
        double TC = columnas /3;  // tercia columnas
        
        for (int i = 0; i < filas; i++) {
            for (int j = 0; j < columnas; j++) {
                
                if ( i >= 0   && j >= 0  && i <= TF  && j <= TC ) 
                {
                    patrones[0] += matrizImg[i][j];
                } 
                else if ( i >= 0   && j >= TC  && i <= TF  && j <= 2*TC ) 
                {
                    patrones[1] += matrizImg[i][j];
                } 
                else if ( i >= 0   && j >= 2*TC  && i <= TF  && j <= 3*TC ) 
                {
                    patrones[2] += matrizImg[i][j];
                }
                else if ( i >= TF   && j >= 0  && i <= 2*TF && j <= TC )  
                {
                    patrones[3] += matrizImg[i][j];
                }
                else if ( i >= TF   && j >= TC  && i <= 2*TF && j <= 2*TC )  
                {
                    patrones[4] += matrizImg[i][j];
                }
                else if ( i >= TF   && j >= 2*TC  && i <= 2*TF && j <= 3*TC )  
                {
                    patrones[5] += matrizImg[i][j];
                }
                else if ( i >= 2*TF && j >= 0  && i <= 3*TF && j <= TC ) 
                {
                    patrones[6] += matrizImg[i][j];
                }
                else if ( i >= 2*TF && j >= TC  && i <= 3*TF && j <= 2*TC ) 
                {
                    patrones[7] += matrizImg[i][j];
                }
                else if ( i >= 2*TF && j >= 2*TC  && i <= 3*TF && j <= 3*TC ) 
                {
                    patrones[8] += matrizImg[i][j];
                }
                
            }
        }
        double n = TF * TC;
        
        BigDecimal res;
        
        for (int i = 0; i < 9; i++) {
            res = new BigDecimal(patrones[i] /n).setScale(4, BigDecimal.ROUND_UP);
            patrones[i] =res.doubleValue();
        }

    }
    
    private void convertirImagenAMatriz() {
        filas = bufferedImage.getHeight();
        columnas = bufferedImage.getWidth();

        matrizImg = new double[filas][columnas];
        matrizImg_R = new double[filas][columnas];
        matrizImg_G = new double[filas][columnas];
        matrizImg_B = new double[filas][columnas];
        double r;
        double g;
        double b;

        WritableRaster raster = bufferedImage.getRaster();
        int numBandas = raster.getNumBands();

        for (int i = 0; i < filas; i++) {
            for (int j = 0; j < columnas; j++) {
                if (numBandas == 3) {
                    r = raster.getSampleDouble(j, i, 0);
                    g = raster.getSampleDouble(j, i, 1);
                    b = raster.getSampleDouble(j, i, 2);

                    matrizImg[i][j] = (r + g + b) / 3;
                    matrizImg_R[i][j] = r;
                    matrizImg_G[i][j] = g;
                    matrizImg_B[i][j] = b;
                }
                if (numBandas == 1) {
                    matrizImg[i][j] = raster.getSampleDouble(j, i, 0);
                    matrizImg_R[i][j] = 255;
                    matrizImg_G[i][j] = 255;
                    matrizImg_B[i][j] = 255;
                }
            }
        }
    }

    public double[] getPatrones() {
        return patrones;
    }
}
