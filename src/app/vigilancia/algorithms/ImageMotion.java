
package app.vigilancia.algorithms;

import java.util.Vector;


public class ImageMotion {
    
    private Vector<Pattern> basedPatterns;
    private int error = 12;
    private double similitud = 6;
    private int posicion=-1;
    
    public ImageMotion(Vector<Pattern> basedPatterns) {
        this.basedPatterns = basedPatterns;
    }
    
    public boolean isMotionDetect(Vector<Pattern> searchPatterns) {
        
        for (int i = 0; i < searchPatterns.size(); i++) {
            for (int j = 0; j < basedPatterns.size(); j++) {
                
                double search[] = searchPatterns.get(i).getPatrones();
                double based[]  = basedPatterns.get(j).getPatrones();
                
                if (  matching(based, search) ) {
                    posicion=i;
                    System.out.println("Matching!");
                    return true;
                }
            }
        }
        
        return false;
    }
    
    private boolean matching(double[] p1, double[] p2) {
 
        if ( calculaError(p1, p2) < error ) {
            return false;
        } else {
            return true;
        }
    }

    public int getPos(){
        return posicion;
    }
    
    public float calculaError(double[] patron1, double[] patron2) {

        int   sumaValores = 0;
        float porcentaje = 0;           // [0- 100]
        double[] valores = new double[9];
        double delta = 0;

        // Verificando diferencia en similitud
        for (int i = 0; i < 9; i++) {
           
            delta =   Math.abs(patron1[i] - patron2[i]);

            if (delta <= similitud) {
                valores[i] = 0;
            } else {
                valores[i] = 1;
            }
        }

        // Calculando porcentaje 
        for (int i = 0; i < 9; i++) {
            sumaValores += valores[i];
        }

        porcentaje = (sumaValores * 100) / 9;
        //System.out.println("Error: "  + porcentaje);
        return porcentaje;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public double getSimilitud() {
        return similitud;
    }

    public void setSimilitud(int similitud) {
        this.similitud = similitud;
    }
    
    
    
}