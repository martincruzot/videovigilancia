
package app.vigilancia.views;

import java.awt.Component;
import java.awt.Image;
import javax.media.Buffer;
import javax.media.CaptureDeviceInfo;
import javax.media.Manager;
import javax.media.MediaLocator;
import javax.media.Player;
import javax.media.cdm.CaptureDeviceManager;
import javax.media.control.FrameGrabbingControl;
import javax.media.format.VideoFormat;
import javax.media.util.BufferToImage;
import javax.swing.JPanel;


public class Camera {
    
    private JPanel renderPanel;
    private static Player player;
    private MediaLocator mediaLocator;
    private CaptureDeviceInfo deviceInfo;
    
    public Camera(JPanel renderPanel) {
        this.renderPanel = renderPanel;
        
        for (int i = 0; i < CaptureDeviceManager.getDeviceList().size(); i++) {

            System.out.println(((CaptureDeviceInfo) CaptureDeviceManager.getDeviceList().get(i)).getName());
        }

        mediaLocator = new MediaLocator("vfw://0");
        
        try {
            Component component;
            player = Manager.createRealizedPlayer(mediaLocator);
            player.start();
            if ((component = player.getVisualComponent()) != null) {
                component.setSize(renderPanel.getWidth(), renderPanel.getHeight());
                renderPanel.add(component);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    public Image getCurrentFrame() {
        
        FrameGrabbingControl fgc = (FrameGrabbingControl) player.getControl("javax.media.control.FrameGrabbingControl");
        Buffer buffer = fgc.grabFrame();
        BufferToImage  bufferToImage = new BufferToImage((VideoFormat) buffer.getFormat());
        Image image = bufferToImage.createImage(buffer);
        
        /*
        if (image == null) {
            System.out.println("No se capturo imagen");
        } 
        else {
            System.out.println("Se ha capturado imagen");
        }
        */
        return image;
    }
}
