package app.vigilancia.views;

import app.vigilancia.algorithms.*;
import java.awt.Graphics2D;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class AplicationFrame extends javax.swing.JFrame {

    private Camera camera;
    private ArrayList<Image> captures;
    private ArrayList<JLabel> renderCaptures;
    private float currenTime = 0;
    private int numCaptures = 0;

    private ImageMotion motion;

    private static final int DELTA_TIME = 250; // milisegundos
    private static final int NUM_CAPTURES = 4;

    Timer timer;
    Timer timer2;
    
    public AplicationFrame() {
        
        try{
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        }catch( ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e){
        } 
        
        initComponents();
        camera = new Camera(this.pnlRenderPanel);
        captures = new ArrayList<>();

        // Agreagando render de las capturas
        renderCaptures = new ArrayList<>();
        renderCaptures.add(this.lblCapture1);
        renderCaptures.add(this.lblCapture2);
        renderCaptures.add(this.lblCapture3);
        renderCaptures.add(this.lblCapture4);

        timer = new Timer(DELTA_TIME, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                //addLog("Capture frame: " + numCaptures);
                if (numCaptures == NUM_CAPTURES) {

                    if (motion != null) {
                        Vector<Pattern> patterns = new Vector<Pattern>();
                        for (int i = 0; i < 4; i++) {
                            patterns.add(new Pattern(captures.get(i)));
                        }
                        if (motion.isMotionDetect(patterns)) {
                            saveImage(captures.get(motion.getPos()));
                            System.out.println("Se ha detectado movimiento!");
                            lblState.setText("Se ha detectado movimiento");
                        }

                        for (int i = 0; i < 4; i++) {
                            addLog(Arrays.toString(patterns.get(i).getPatrones()));
                        }

                    } else {
                        System.out.println("No se ha cargado patrones base");
                    }

                    addLog("Se muestra frames capturados!");
                    showCaptures();
                    captures.clear();
                    numCaptures = 0;

                } else {
                    lblState.setText("No se ha detectado movimiento...");
                    Image image = camera.getCurrentFrame();
                    captures.add(image);
                    numCaptures++;
                }
            }
        });
        
        timer2 = new Timer(1000, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                Calendar fecha = new GregorianCalendar();
                int anho = fecha.get(Calendar.YEAR);
                int mes = fecha.get(Calendar.MONTH);
                int dia = fecha.get(Calendar.DAY_OF_MONTH);
                int hora = fecha.get(Calendar.HOUR_OF_DAY);
                int minuto = fecha.get(Calendar.MINUTE);
                int segundo = fecha.get(Calendar.SECOND);
                campoHora.setText(" "+dia+"/"+mes+"/"+anho+ " " + hora + ":" + minuto + ":" + segundo);
            }      
        }
        );
        timer2.start();
    }
    

    private void saveImage(Image img){
        String direccion_imagen;
        File nueva_foto=null;
        
        try {
            BufferedImage muestra = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_RGB);
            Graphics2D g2 = muestra.createGraphics();
            g2.drawImage(img, null, null);
            g2.dispose();
            String dir = "";

            File almacen = new File(System.getProperty("user.dir") + "/Fotos");// Creamos el archivo donde se guardarÃ¡n las fotos capturadas
            if (almacen.mkdir()) {
                System.out.println("Carpeta Fotos Creado");
            }
            try {
                dir = System.getProperty("user.dir") + "\\Fotos" + "\\" + getInfo() + ".jpg";
                direccion_imagen = dir;
                nueva_foto = new File(dir);
                System.out.println(dir);
                try {
                    if (nueva_foto.createNewFile()) {
                        System.out.println("El fichero se ha creado correctamente");
                    } else {
                        System.out.println("No ha podido ser creado el fichero");
                    }
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            } catch (Exception e13) {
                JOptionPane.showMessageDialog(null, e13);
            }
            try {
                ImageIO.write(muestra, "jpg", nueva_foto);
            } catch (IOException e1) {
                System.out.println("Error de escritura");
            }
        } catch (Exception e12) {
            System.out.println(e12);
        }
    }
    
    private String getInfo(){
        Calendar fecha = new GregorianCalendar();
        int anho = fecha.get(Calendar.YEAR);
        int mes = fecha.get(Calendar.MONTH);
        int dia = fecha.get(Calendar.DAY_OF_MONTH);
        int hora = fecha.get(Calendar.HOUR_OF_DAY);
        int minuto = fecha.get(Calendar.MINUTE);
        int segundo = fecha.get(Calendar.SECOND);
        return "Captura" + "_" +dia+"-"+mes+"-"+anho+ "_" + hora + "-" + minuto + "-" + segundo;
    }
    
    private void showCaptures() {
        ImageIcon icon;
        Image image;
        JLabel label;

        for (int i = 0; i < renderCaptures.size(); i++) {

            image = captures.get(i);
            label = renderCaptures.get(i);

            icon = new ImageIcon(image);
            Icon scaleImage = new ImageIcon(icon.getImage().getScaledInstance(label.getWidth(),
                    label.getHeight(),
                    Image.SCALE_DEFAULT));
            label.setIcon(scaleImage);
        }

        this.repaint();
    }

    private void addLog(String message) {
        message = this.txaLogs.getText() + "\n" + message;
        this.txaLogs.setText(message);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlRenderPanel = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        lblCapture1 = new javax.swing.JLabel();
        lblCapture2 = new javax.swing.JLabel();
        lblCapture3 = new javax.swing.JLabel();
        lblCapture4 = new javax.swing.JLabel();
        btnInitSystem = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        txaLogs = new javax.swing.JTextArea();
        btnSaveScene = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        campoHora = new javax.swing.JTextField();
        lblState = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        pnlRenderPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        javax.swing.GroupLayout pnlRenderPanelLayout = new javax.swing.GroupLayout(pnlRenderPanel);
        pnlRenderPanel.setLayout(pnlRenderPanelLayout);
        pnlRenderPanelLayout.setHorizontalGroup(
            pnlRenderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 395, Short.MAX_VALUE)
        );
        pnlRenderPanelLayout.setVerticalGroup(
            pnlRenderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 298, Short.MAX_VALUE)
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblCapture1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.add(lblCapture1, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 13, 215, 153));

        lblCapture2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.add(lblCapture2, new org.netbeans.lib.awtextra.AbsoluteConstraints(237, 13, 223, 153));

        lblCapture3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.add(lblCapture3, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 177, 215, 150));

        lblCapture4.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.add(lblCapture4, new org.netbeans.lib.awtextra.AbsoluteConstraints(237, 177, 220, 150));

        btnInitSystem.setFont(new java.awt.Font("Khmer UI", 0, 11)); // NOI18N
        btnInitSystem.setText("Iniciar vigilancia");
        btnInitSystem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInitSystemActionPerformed(evt);
            }
        });

        txaLogs.setEditable(false);
        txaLogs.setColumns(20);
        txaLogs.setFont(new java.awt.Font("Khmer UI", 0, 13)); // NOI18N
        txaLogs.setRows(5);
        txaLogs.setText("\n");
        jScrollPane1.setViewportView(txaLogs);

        btnSaveScene.setFont(new java.awt.Font("Khmer UI", 0, 11)); // NOI18N
        btnSaveScene.setText("Guardar escena");
        btnSaveScene.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveSceneActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Khmer UI", 0, 11)); // NOI18N
        jLabel1.setText("Fecha y Hora:");

        campoHora.setEditable(false);
        campoHora.setFont(new java.awt.Font("Khmer UI", 0, 11)); // NOI18N
        campoHora.setBorder(null);

        lblState.setText("jLabel2");
        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(pnlRenderPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(campoHora, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(lblState, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 472, Short.MAX_VALUE)
                                .addContainerGap())
                            .addGroup(layout.createSequentialGroup()
                                .addGap(52, 52, 52)
                                .addComponent(btnSaveScene, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnInitSystem, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(19, 19, 19))))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 343, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(campoHora, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(pnlRenderPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnInitSystem)
                        .addComponent(btnSaveScene))
                    .addComponent(lblState))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 185, Short.MAX_VALUE)
                .addGap(21, 21, 21))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnInitSystemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInitSystemActionPerformed
        timer.start();

    }//GEN-LAST:event_btnInitSystemActionPerformed

    private void btnSaveSceneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveSceneActionPerformed

        Vector<Pattern> patterns = new Vector<Pattern>();

        for (int i = 0; i < 4; i++) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                Logger.getLogger(AplicationFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
            patterns.add(new Pattern(camera.getCurrentFrame()));
        }

        motion = new ImageMotion(patterns);

        for (int i = 0; i < 4; i++) {
            this.addLog(Arrays.toString(patterns.get(i).getPatrones()));
        }

    }//GEN-LAST:event_btnSaveSceneActionPerformed

    public void storingInfo(){
        Conexion con=new Conexion();
        try{
            
            Statement st = (Statement) con.getConexion().createStatement(); 

            st.executeUpdate("INSERT INTO Historial() " + "VALUES (3, 75, 6, 25, 18.50)");

            con.getConexion().close();
        }

        catch (SQLException ex) {
            
         }         
    }
    
    
    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AplicationFrame().setVisible(true);
                System.out.println("Init GUI");
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnInitSystem;
    private javax.swing.JButton btnSaveScene;
    private javax.swing.JTextField campoHora;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblCapture1;
    private javax.swing.JLabel lblCapture2;
    private javax.swing.JLabel lblCapture3;
    private javax.swing.JLabel lblCapture4;
    private javax.swing.JLabel lblState;
    private javax.swing.JPanel pnlRenderPanel;
    private javax.swing.JTextArea txaLogs;
    // End of variables declaration//GEN-END:variables
}
